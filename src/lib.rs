//! Low level wrapper over raw GL calls.
//!
//! No particular focus on safety, main point
//! of this is to provide a slightly more ergonomic interface
//! over rust-gl

extern crate gl as gl_raw;

#[derive(PartialEq, Debug, Clone, Copy)]
pub enum Buffer {
    Color,
}

impl Buffer {
    pub fn to_enum(self) -> gl_raw::types::GLenum {
        match self {
            Buffer::Color => gl_raw::COLOR,
        }
    }
}
/// [See official documentation](https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glClearBuffer.xhtml)
pub fn ClearBufferfv(buffer: Buffer, draw_buffer: i32, values: &[f32]) {
    unsafe {
        gl_raw::ClearBufferfv(buffer.to_enum(), draw_buffer, &values[0] as *const f32);
    }
}
